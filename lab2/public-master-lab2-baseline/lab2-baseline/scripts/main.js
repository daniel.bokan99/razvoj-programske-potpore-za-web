setTimeout(function(){
    getData();
}, 3000);

function sleep(time) {
    return new Promise(resolve => setTimeout(resolve, time));
}


function onLoadFunkcija(){
    let cartItems = document.getElementById("cart-items");
    if(typeof sessionStorage.clickCount === "undefined"){
        sessionStorage.clickCount = 0;
    }
    cartItems.innerHTML = sessionStorage.clickCount;
}

/*function onUnloadFunkcija(){
    let cartItems = document.getElementById("cart-items");
    localStorage.clickCount = 0;
    cartItems.innerHTML = localStorage.clickCount;
}*/

function addToCart(){
    let cartItems = document.getElementById("cart-items");
    if(sessionStorage.clickCount){
        sessionStorage.clickCount = Number(sessionStorage.clickCount) + 1;
    }else{
        sessionStorage.clickCount = 1;
    }
    cartItems.innerHTML = sessionStorage.clickCount;
}

let getData = async function () {
    let response = await fetch("https://wimlab2.azurewebsites.net/categories");
    let data = await response.json();
    addCategories(data);
}

let addCategories = async function (categories) {
    let main = document.querySelector('main');
    let categoryTemplate = document.querySelector('#category-template');

    for (let index = 0; index < categories.length; index++) {
        let category = categoryTemplate.content.cloneNode(true);
        let categoryTitleElement = category.querySelector('.decorated-title > span');
        categoryTitleElement.textContent = categories[index].name;
        main.appendChild(category);
        
        var url = "https://wimlab2.azurewebsites.net/products?categoryId=" + (index + 1);
        var productsResponse = await fetch(url);
        var products = await productsResponse.json();

        productData(products, index);
        
        await sleep(3000);

    }
    document.querySelector('.loader').classList.replace('loader', 'hidden');
};
function productData(products, index) {
    var productTemplate = document.querySelector('#product-template');
    var gallery = document.querySelectorAll('.gallery');

    for (const productIndex of products) {
        const product = productTemplate.content.cloneNode(true);
        const cartButton = product.querySelector('.cart-btn');
        product.querySelector('.photo-box').setAttribute(
            'data-id', productIndex.categoryId);
        product.querySelector('.photo-box-image').src = productIndex.imageUrl;
        product.querySelector('.photo-box-title').innerHTML = productIndex.name;
        cartButton.setAttribute('price', productIndex.price);
        cartButton.setAttribute('onclick', 'addToCart()');
        gallery[index].appendChild(product);
    }
}