const express = require('express');
const router = express.Router();

router.get('/', function(req, res, next){
    res.render('Dodatak', {
        title: "Dodatak",
        linkActive: "dodatak"
    });
});

module.exports = router;