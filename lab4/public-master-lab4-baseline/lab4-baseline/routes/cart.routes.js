const express = require('express');
const router = express.Router();
const cart = require('../models/CartModel')


//ZADATAK prikaz košarice uz pomoć cart.ejs
router.get('/', function (req, res, next) {
    res.render('Cart',{
        title: "Cart",
        linkActive: "Cart",
        user: req.session.user,
        cart: req.session.cart,
        err: undefined
    });
});

//ZADATAK: dodavanje jednog artikla u košaricu
router.get('/add/:id', async function (req, res, next) {
    let id = req.params.id;
    //console.log(req.session.cart);
    if(req.session.cart === undefined){
        req.session.cart = cart.createCart();
    }

    await cart.addItemToCart(req.session.cart, id, 1);
    
    res.end();
    //console.log(req.session.cart.totalAmount);
  
});

//ZADATAK: brisanje jednog artikla iz košarice
router.get('/remove/:id', async function (req, res, next) {
    

    await cart.removeItemFromCart(req.session.cart, req.params.id, 1);
    if(req.session.cart.totalAmount == 0){
        req.session.cart = undefined;
    }
    //console.log(req.session.cart.totalAmount);
    res.end();
  
});

router.get('/removeAll/:id/', async function(req, res, next){
    let id = req.params.id;
    var json = JSON.parse(JSON.stringify(req.session.cart));
    var kolicina = json.items[id].quantity;
    //console.log(kolicina);
    
    await cart.removeItemFromCart(req.session.cart, req.params.id, kolicina);
    
    if(req.session.cart.totalAmount == 0){
        req.session.cart = undefined;
    }
    res.end();
});
module.exports = router;