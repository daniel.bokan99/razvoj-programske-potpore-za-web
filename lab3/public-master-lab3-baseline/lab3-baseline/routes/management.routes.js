var express = require('express');
var router = express.Router();
const db = require("../db");


router.get('/', function(req, res, next){
    res.render('management',{
        title: 'Management',
        linkActive: 'management'
    });
});

router.get('/additem', function(req, res, next){
    res.render('additem',{
        title: 'Add item',
        linkActive: 'management'
    });
});

router.post('/additem', async function(req, res, next){
    console.log(req.body);
    try{
        let newItem;
        newItem.name = req.body.name;
        newItem.price = req.body.price;
        newItem.category = req.body.category;
        newItem.url = req.body.url;

        let insert = await db.query("INSERT INTO Inventory (name, price, category, imageurl) VALUES('${newItem.name}', '${newItem.price}', '${newItem.category}', '${newItem.imageUrl}')");
        


        res.redirect('/management');

    }catch(err){
        res.render('error',{
            title: 'Error',
            linkActive: 'error',
            errors: JSON.stringify(err),
            errDB: "name"
        });
    }
});


module.exports = router;