var express = require("express");
var router = express.Router();
const db = require("../db");

router.get('/', async function(req, res, next){
    let kategorija = await db.query("SELECT * FROM categories");
    let inventar = await db.query("SELECT * FROM inventory");

    res.render("order", {
        title: "Order",
        linkActive: "order",
        categories: kategorija.rows,
        inventory: inventar.rows
    });
});

module.exports = router;